import pandas as pd

import Edge
import Node


def getDFG(filePath, separator):
    df = pd.read_csv(filePath, sep=separator, header=None, error_bad_lines=False)

    nodes = []

    for index, row in df.iterrows():

        node = Node.Node()

        inputs = []

        rowSize = len(row)

        if rowSize != 5:
            # ERROR: Incorrect Row Size
            print("ERROR: Incorrect Row Size")
            exit(0)

        # Check if special case
        isSpecialCase = pd.isnull(row[3]) or pd.isnull(row[4])

        # Output
        node.output = row[0]

        # Inputs and Operator
        if not isSpecialCase:
            node.operator = row[3]
            inputs.append(row[2])
            inputs.append(row[4])
        else:
            node.operator = "*"
            thirdItem = row[2]
            firstCharacter = thirdItem[0]
            if firstCharacter == "-":
                inputs.append("-1")
                inputs.append(thirdItem[1:])
            elif firstCharacter == "+":
                inputs.append("1")
                inputs.append(thirdItem[1:])
            else:
                inputs.append("1")
                inputs.append(thirdItem)

        for inputValue in inputs:
            edge = Edge.Edge()

            edge.endNode = node
            edge.value = inputValue

            for existingNode in nodes:
                if inputValue == existingNode.output:
                    edge.startNode = existingNode
                    existingNode.outEdges.append(edge)
                    break

            node.inEdges.append(edge)

        nodes.append(node)

    return nodes
