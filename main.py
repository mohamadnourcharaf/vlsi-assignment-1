import Parameters
import DataManager
import ASAPAlgorithm
import ALAPAlgorithm
import FDLSAlgorithm


def start():

    # Get Nodes
    nodes = DataManager.getDFG(Parameters.fileName, Parameters.separator)

    # ASAP
    ASAPAlgorithm.scheduleWithASAP(nodes)
    ASAPAlgorithm.printASAPResult(nodes)

    # ALAP
    timeConstraint = nodes[-1].asapTime
    ALAPAlgorithm.scheduleWithALAP(nodes,timeConstraint)
    ALAPAlgorithm.printALAPResult(nodes)

    # FDLS
    FDLSAlgorithm.scheduleWithFDLS(nodes,timeConstraint,Parameters.numberOfMultiplierFunctionalUnits,Parameters.numberOfAdditionFunctionalUnits,Parameters.numberOfSubtractionFunctionalUnits)
    FDLSAlgorithm.printFDLSResult(nodes)

start()
