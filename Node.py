class Node:

    def __init__(self):
        self.inEdges = []
        self.outEdges = []
        self.output = None
        self.operator = None
        self.asapTime = None
        self.alapTime = None
        self.mobility = None
        self.isFixed = False
        self.min_deferred_c_step = None
        self.minForce = None
        self.fdlsTime = None