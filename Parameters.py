

diffeqFileName = "diffeq.dfg"
ellipticFileName = "elliptic.dfg"
separator = " "

numberOfMultiplierFunctionalUnits = 3
numberOfAdditionFunctionalUnits = 2
numberOfSubtractionFunctionalUnits = 1
fileName = diffeqFileName